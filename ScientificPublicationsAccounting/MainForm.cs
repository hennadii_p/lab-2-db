﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificPublicationsAccounting
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            dataGridView1.AutoGenerateColumns = true;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.Discipline". При необходимости она может быть перемещена или удалена.
            this.disciplineTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.Discipline);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.Topic". При необходимости она может быть перемещена или удалена.
            this.topicTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.Topic);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.University". При необходимости она может быть перемещена или удалена.
            this.universityTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.University);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.Publication". При необходимости она может быть перемещена или удалена.
            this.publicationTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.Publication);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.Scientist". При необходимости она может быть перемещена или удалена.
            this.scientistTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.Scientist);

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            scientistTableAdapter.Update(scientificPublicationsAccountingDataSet);
            publicationTableAdapter.Update(scientificPublicationsAccountingDataSet);
            disciplineTableAdapter.Update(scientificPublicationsAccountingDataSet);
            universityTableAdapter.Update(scientificPublicationsAccountingDataSet);
            topicTableAdapter.Update(scientificPublicationsAccountingDataSet);
        }

        private void scientistsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainBindingNavigator.BindingSource = scientistBindingSource;
            dataGridView1.DataSource = scientistBindingSource;
            label1.Text = "Scientists";
        }

        private void publicationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainBindingNavigator.BindingSource = publicationBindingSource;
            dataGridView1.DataSource = publicationBindingSource;
            label1.Text = "Publications";
        }

        private void universitiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainBindingNavigator.BindingSource = universityBindingSource;
            dataGridView1.DataSource = universityBindingSource;
            label1.Text = "Universities";
        }

        private void topicsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainBindingNavigator.BindingSource = topicBindingSource;
            dataGridView1.DataSource = topicBindingSource;
            label1.Text = "Topics";
        }

        private void disciplinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MainBindingNavigator.BindingSource = disciplineBindingSource;
            dataGridView1.DataSource = disciplineBindingSource;
            label1.Text = "Disciplines";
        }

        private void scientistsButton_Click(object sender, EventArgs e)
        {
            var s = new Scientists();
            s.ShowDialog();
            scientistTableAdapter.Fill(scientificPublicationsAccountingDataSet.Scientist);
            universityTableAdapter.Fill(scientificPublicationsAccountingDataSet.University);
        }
    }
}

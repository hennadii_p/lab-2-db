﻿namespace ScientificPublicationsAccounting
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.scientificPublicationsAccountingDataSet = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4();
            this.bindingSourceScientists = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scientistsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.publicationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.universitiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topicsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disciplinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.MainBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.scientistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.scientistTableAdapter = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4TableAdapters.ScientistTableAdapter();
            this.publicationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.publicationTableAdapter = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4TableAdapters.PublicationTableAdapter();
            this.universityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.universityTableAdapter = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4TableAdapters.UniversityTableAdapter();
            this.topicBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.topicTableAdapter = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4TableAdapters.TopicTableAdapter();
            this.disciplineBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.disciplineTableAdapter = new ScientificPublicationsAccounting.ScientificPublicationsAccountingDataSet4TableAdapters.DisciplineTableAdapter();
            this.scientistsButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scientificPublicationsAccountingDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceScientists)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainBindingNavigator)).BeginInit();
            this.MainBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scientistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.universityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topicBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.disciplineBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 54);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 304);
            this.dataGridView1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "label1";
            // 
            // scientificPublicationsAccountingDataSet
            // 
            this.scientificPublicationsAccountingDataSet.DataSetName = "ScientificPublicationsAccountingDataSet";
            this.scientificPublicationsAccountingDataSet.Locale = new System.Globalization.CultureInfo("ru-UA");
            this.scientificPublicationsAccountingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingSourceScientists
            // 
            this.bindingSourceScientists.DataSource = this.scientificPublicationsAccountingDataSet;
            this.bindingSourceScientists.Position = 0;
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scientistsToolStripMenuItem,
            this.publicationsToolStripMenuItem,
            this.universitiesToolStripMenuItem,
            this.topicsToolStripMenuItem,
            this.disciplinesToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // scientistsToolStripMenuItem
            // 
            this.scientistsToolStripMenuItem.Name = "scientistsToolStripMenuItem";
            this.scientistsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.scientistsToolStripMenuItem.Text = "Scientists";
            this.scientistsToolStripMenuItem.Click += new System.EventHandler(this.scientistsToolStripMenuItem_Click);
            // 
            // publicationsToolStripMenuItem
            // 
            this.publicationsToolStripMenuItem.Name = "publicationsToolStripMenuItem";
            this.publicationsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.publicationsToolStripMenuItem.Text = "Publications";
            this.publicationsToolStripMenuItem.Click += new System.EventHandler(this.publicationsToolStripMenuItem_Click);
            // 
            // universitiesToolStripMenuItem
            // 
            this.universitiesToolStripMenuItem.Name = "universitiesToolStripMenuItem";
            this.universitiesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.universitiesToolStripMenuItem.Text = "Universities";
            this.universitiesToolStripMenuItem.Click += new System.EventHandler(this.universitiesToolStripMenuItem_Click);
            // 
            // topicsToolStripMenuItem
            // 
            this.topicsToolStripMenuItem.Name = "topicsToolStripMenuItem";
            this.topicsToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.topicsToolStripMenuItem.Text = "Topics";
            this.topicsToolStripMenuItem.Click += new System.EventHandler(this.topicsToolStripMenuItem_Click);
            // 
            // disciplinesToolStripMenuItem
            // 
            this.disciplinesToolStripMenuItem.Name = "disciplinesToolStripMenuItem";
            this.disciplinesToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.disciplinesToolStripMenuItem.Text = "Disciplines";
            this.disciplinesToolStripMenuItem.Click += new System.EventHandler(this.disciplinesToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.databaseToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(671, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "1";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Добавить";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Удалить";
            // 
            // MainBindingNavigator
            // 
            this.MainBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.MainBindingNavigator.BindingSource = this.bindingSourceScientists;
            this.MainBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.MainBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.MainBindingNavigator.Dock = System.Windows.Forms.DockStyle.None;
            this.MainBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.MainBindingNavigator.Location = new System.Drawing.Point(12, 361);
            this.MainBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.MainBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.MainBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.MainBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.MainBindingNavigator.Name = "MainBindingNavigator";
            this.MainBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.MainBindingNavigator.Size = new System.Drawing.Size(263, 25);
            this.MainBindingNavigator.TabIndex = 1;
            this.MainBindingNavigator.Text = "bindingNavigator1";
            // 
            // scientistBindingSource
            // 
            this.scientistBindingSource.DataMember = "Scientist";
            this.scientistBindingSource.DataSource = this.bindingSourceScientists;
            // 
            // scientistTableAdapter
            // 
            this.scientistTableAdapter.ClearBeforeFill = true;
            // 
            // publicationBindingSource
            // 
            this.publicationBindingSource.DataMember = "Publication";
            this.publicationBindingSource.DataSource = this.bindingSourceScientists;
            // 
            // publicationTableAdapter
            // 
            this.publicationTableAdapter.ClearBeforeFill = true;
            // 
            // universityBindingSource
            // 
            this.universityBindingSource.DataMember = "University";
            this.universityBindingSource.DataSource = this.bindingSourceScientists;
            // 
            // universityTableAdapter
            // 
            this.universityTableAdapter.ClearBeforeFill = true;
            // 
            // topicBindingSource
            // 
            this.topicBindingSource.DataMember = "Topic";
            this.topicBindingSource.DataSource = this.bindingSourceScientists;
            // 
            // topicTableAdapter
            // 
            this.topicTableAdapter.ClearBeforeFill = true;
            // 
            // disciplineBindingSource
            // 
            this.disciplineBindingSource.DataMember = "Discipline";
            this.disciplineBindingSource.DataSource = this.bindingSourceScientists;
            // 
            // disciplineTableAdapter
            // 
            this.disciplineTableAdapter.ClearBeforeFill = true;
            // 
            // scientistsButton
            // 
            this.scientistsButton.Location = new System.Drawing.Point(315, 364);
            this.scientistsButton.Name = "scientistsButton";
            this.scientistsButton.Size = new System.Drawing.Size(141, 23);
            this.scientistsButton.TabIndex = 8;
            this.scientistsButton.Text = "Scientists";
            this.scientistsButton.UseVisualStyleBackColor = true;
            this.scientistsButton.Click += new System.EventHandler(this.scientistsButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 442);
            this.Controls.Add(this.scientistsButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MainBindingNavigator);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dataGridView1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scientificPublicationsAccountingDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceScientists)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainBindingNavigator)).EndInit();
            this.MainBindingNavigator.ResumeLayout(false);
            this.MainBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scientistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.publicationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.universityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topicBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.disciplineBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private ScientificPublicationsAccountingDataSet4 scientificPublicationsAccountingDataSet;
        private System.Windows.Forms.BindingSource bindingSourceScientists;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scientistsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem publicationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem universitiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem topicsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disciplinesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.BindingNavigator MainBindingNavigator;
        private System.Windows.Forms.BindingSource scientistBindingSource;
        private ScientificPublicationsAccountingDataSet4TableAdapters.ScientistTableAdapter scientistTableAdapter;
        private System.Windows.Forms.BindingSource publicationBindingSource;
        private ScientificPublicationsAccountingDataSet4TableAdapters.PublicationTableAdapter publicationTableAdapter;
        private System.Windows.Forms.BindingSource universityBindingSource;
        private ScientificPublicationsAccountingDataSet4TableAdapters.UniversityTableAdapter universityTableAdapter;
        private System.Windows.Forms.BindingSource topicBindingSource;
        private ScientificPublicationsAccountingDataSet4TableAdapters.TopicTableAdapter topicTableAdapter;
        private System.Windows.Forms.BindingSource disciplineBindingSource;
        private ScientificPublicationsAccountingDataSet4TableAdapters.DisciplineTableAdapter disciplineTableAdapter;
        private System.Windows.Forms.Button scientistsButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScientificPublicationsAccounting
{
    public partial class Scientists : Form
    {
        public Scientists()
        {
            InitializeComponent();
        }

        private void scientistBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.scientistBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.scientificPublicationsAccountingDataSet);

        }

        private void universityBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.universityBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.scientificPublicationsAccountingDataSet);

        }

        private void Scientists_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.Scientist". При необходимости она может быть перемещена или удалена.
            this.scientistTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.Scientist);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scientificPublicationsAccountingDataSet.University". При необходимости она может быть перемещена или удалена.
            this.universityTableAdapter.Fill(this.scientificPublicationsAccountingDataSet.University);

        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "Do you want to save?", 
                "Data changing", 
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                scientistBindingSource.EndEdit();
                universityBindingSource.EndEdit();
                scientistTableAdapter.Update(scientificPublicationsAccountingDataSet);
                universityTableAdapter.Update(scientificPublicationsAccountingDataSet);
            }
        }

        private void Scientists_FormClosing(object sender, FormClosingEventArgs e)
        {
            scientistTableAdapter.Update(scientificPublicationsAccountingDataSet);
            universityTableAdapter.Update(scientificPublicationsAccountingDataSet);
        }
    }
}

﻿CREATE TABLE [dbo].[Scientist]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[First_name] VARCHAR(MAX) NOT NULL,
	[Last_name] VARCHAR(MAX) NOT NULL,
	[Middle_name] VARCHAR(MAX) NULL,
	[University] INT NOT NULL, 
    CONSTRAINT [FK_Scientist_ToTable_University] FOREIGN KEY ([University]) REFERENCES [University]([Id])
)

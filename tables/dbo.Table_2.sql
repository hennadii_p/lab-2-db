﻿CREATE TABLE [dbo].[University]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR(MAX) NOT NULL
)

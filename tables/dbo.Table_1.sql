﻿CREATE TABLE [dbo].[Publication]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Topic] INT NOT NULL,
	[Publishing_date] DATE NOT NULL,
	[Name] VARCHAR(MAX) NOT NULL,
	[Rating] INT NOT NULL,
	[Conference] INT NOT NULL, 
    CONSTRAINT [FK_Publication_ToTableTopic] FOREIGN KEY ([Topic]) REFERENCES [Topic]([Id]), 
    CONSTRAINT [FK_Publication_ToTable_Conference] FOREIGN KEY ([Conference]) REFERENCES [Conference]([Id])
)
